Crafty.init(600, 400, document.getElementById('game'))
Crafty.background('#FFFF00')
var polygon = Crafty.polygon(0, 200, 400, 200);

var fence_top = Crafty.e('2D, Canvas, Color, fence1, WiredHitBox')
  .attr(
    {
      x: 0,
      y: 0,
      h: 5,
      w: 600
    }
  )
  .color('blue')
  .debugStroke('red')

var fence_floar = Crafty.e('2D, Canvas, Color, fence1, WiredHitBox')
  .attr(
    {
      x: 0,
      y: 395,
      h: 5,
      w: 600
    }
  )
  .color('blue')
  .debugStroke('red')
var fence_left = Crafty.e('2D, Canvas, Color, fence2, WiredHitBox')
  .attr(
    {
      x: 0,
      y: 0,
      h: 400,
      w: 5
    }
  )
  .color('blue')
  .debugStroke('red')
var fence_right = Crafty.e('2D, Canvas, Color, fence2, WiredHitBox')
  .attr(
    {
      x: 595,
      y: 0,
      h: 400,
      w: 5
    }
  )
  .color('blue')
  .debugStroke("red")

function getCircleCoordinates(radius){
	var coordinates = [];
	var increment = 0.5; //the smaller the increment the higher the accuracy
	if(radius <= 0){
		console.log("Error radius must be greater than 0");
		return;
	}
	for(var x = -1 * radius; x < radius; x += increment){
		y = Math.sqrt(radius * radius - x * x);
		coordinates.push(radius + x);
		coordinates.push(radius + y);
	}
	for(var x = radius; x > -1 * radius; x-= increment){
		y = Math.sqrt(radius * radius - x * x);
		coordinates.push(radius + x);
		coordinates.push(radius + -1 * y);
	}
	return coordinates;
} 

Crafty.sprite("imgs/ball.png", {ball: [0, 0, 50, 50]})
var ball = Crafty.e('2D, Canvas, Collision, Motion, WiredHitBox, ball')
  .attr({
    x: 10, 
    y: 20, 
    h: 50,
    w: 50})
  .debugStroke("red")
  var circleCoords = getCircleCoordinates(ball.attr("w") / 2);
  ball.collision(circleCoords)
  .bind("EnterFrame", function(){
    var step = Crafty.e('2D, Canvas, Color')
    .attr({
      x: this.x + 25,
      y: this.y + 25,
      h: 2,
      w: 2
    })
    .color('black')

    let hitDatas
    if(hitDatas = this.hit("wall1")) {
      let hitData = hitDatas[0]
      vel.x -= hitData.nx * hitData.overlap
      vel.y -= hitData.ny * hitData.overlap
    }
  })
var vel = ball.velocity()

vel.x = -50
vel.y = -50

var v1 = new Crafty.math.Vector2D(3, 5)


ball.onHit("fence1",
  function(hitData1){
    vel.y = -vel.y
  });

ball.onHit("fence2",
  function(hitData2){
    vel.x = -vel.x
  })

var wall = Crafty.e("2D, Canvas, Collision, WiredHitBox, wall1")
  .attr({x:0, y:0, w:1000, h:1000})
  .debugStroke("green")
  .collision(
    [200, 100, 100, 300]
  )
  
/*ball.checkHits("wall1").bind("HitOn", 
  function(){
    let hitData = this.hit("wall1")
    if(hitData)
      console.log(hitData.overlap)

  })*/


//console.log(180 / Math.PI * Math.atan2(200, 100))


